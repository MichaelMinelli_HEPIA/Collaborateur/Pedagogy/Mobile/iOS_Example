//
//  JSONDecodable.swift
//  iOS Example
//
//  Created by Michaël Minelli on 24.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import Foundation
import SwiftyJSON

/// Protocol which define what a class must implement for being instantiate with a JSON
public protocol JSONDecodable {
    
    /// Init with JSON object
    ///
    /// - Parameter json: JSON object from SwiftyJSON
    init(json: JSON)
}
