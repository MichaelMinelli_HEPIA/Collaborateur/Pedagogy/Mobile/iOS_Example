//
//  Colors.swift
//  iOS Example
//
//  Created by Michaël Minelli on 25.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import Foundation
import UIKit

/// Self defined colors for the project
public struct Colors {
    public static let swissRed = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
    
    public static let primaryColor = Colors.swissRed
    public static let secondaryColor = UIColor.white
}
