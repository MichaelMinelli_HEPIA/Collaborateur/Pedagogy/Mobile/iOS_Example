//
//  Food.swift
//  iOS Example
//
//  Created by Michaël Minelli on 24.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire


/// Describe a food
open class Food: JSONDecodable {
    
    /// Name of the food
    var name: String = ""
    
    /// Name of the picture file on the server
    var pictureName: String = ""
    
    /// Description of the food
    var description: String = ""
    
    /// Picture loaded from the file
    var picture: UIImage? = nil
    
    required public init(json: JSON) {
        if let name = json["name"].string {
            self.name = name
        }
        
        if let pictureName = json["picture"].string {
            self.pictureName = pictureName
        }
        
        if let description = json["description"].string {
            self.description = description
        }
    }
    
    /// Start the load of the picture from the serveur (Asynchronous)
    ///
    /// - Parameter delegate: Delegate which is call when the load of the picture is finished
    func loadPicture(withDelegate delegate: FoodDelegate) {
        //Encode the picture name for url (for exemple " " become "%20")
        if let urlPictureName = self.pictureName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            AF.request(Constants.serverFoodPicturesDir + urlPictureName, method: .get).response(queue: DispatchQueue.global(qos: .utility)) { response in
                if let data = response.data {
                    //Try to instantiate the picture with received datas
                    self.picture = UIImage(data: data)
                    
                    usleep((arc4random_uniform(1500) + 500) * 1000) //Just for demo purpose (network lag simulation)
                    
                    //Call the delegate on main thread
                    DispatchQueue.main.async {
                        delegate.pictureLoaded(for: self)
                    }
                }
            }
        }
    }
}
