//
//  FoodDelegate.swift
//  iOS Example
//
//  Created by Michaël Minelli on 25.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import Foundation

/// Protocol which define what a class must implement for being called when the picture of the food is loaded
protocol FoodDelegate {
    
    /// Call when the picture download finished
    ///
    /// - Parameter food: Food for which the picture is loaded
    func pictureLoaded(for food: Food)
}
