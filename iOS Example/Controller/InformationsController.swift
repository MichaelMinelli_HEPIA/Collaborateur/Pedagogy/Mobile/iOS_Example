//
//  InformationsController.swift
//  iOS Example
//
//  Created by Michaël Minelli on 24.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import Foundation
import UIKit

class InformationsController: UIViewController {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tvDescription: UITextView!
    
    /// Food to display
    var food: Food?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let food = food {
            img.image = food.picture
            lblName.text = food.name
            tvDescription.text = food.description
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
