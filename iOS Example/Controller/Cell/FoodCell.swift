//
//  FoodCell.swift
//  iOS Example
//
//  Created by Michaël Minelli on 24.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire


/// Describe a TableView Cell for a food element
class FoodCell: UITableViewCell, FoodDelegate {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var aiPictureLoad: UIActivityIndicatorView!
    
    //Current food shown on this cell
    var food: Food?
    
    
    /// Hide picture and show an Activity Indicator
    func hidePicture() {
        img.image = nil
        img.isHidden = true
        
        aiPictureLoad.isHidden = false
        aiPictureLoad.startAnimating()
    }
    
    /// Show the picture in the cell
    ///
    /// - Parameter picture: The picture to show
    func show(_ picture: UIImage) {
        self.img.image = picture
        
        self.aiPictureLoad.isHidden = true
        self.img.isHidden = false
    }
    
    /// Prepare the cell for display
    ///
    /// - Parameter food: Food to display
    func prepare(with food: Food) {
        hidePicture()
        
        self.food = food
        
        lblName.text = food.name
        
        //Test if the picture of the food is already loaded ans if not start the download
        if let picture = food.picture {
            show(picture)
        } else {
            food.loadPicture(withDelegate: self)
        }
    }
    
    func pictureLoaded(for food: Food) {
        if let picture = food.picture {
            if let foodCell = self.food, foodCell === food { //=== Test if two objects have the same reference, we test the reference for being sure that between the time of the load request and the finish the cell was not reallocated to an another food (happend when the user scroll the Table View)
                self.show(picture)
            }
        }
    }
}

