//
//  ViewController.swift
//  iOS Example
//
//  Created by Michaël Minelli on 24.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FoodListController: UITableViewController {
    
    /// All of our database of food is loaded into this array
    var foods: [Food] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Init navigation bar style (red background whith white text)
        if let navigationController = self.navigationController {
            navigationController.navigationBar.backgroundColor = Colors.primaryColor
            navigationController.navigationBar.barTintColor = Colors.primaryColor
            navigationController.navigationBar.titleTextAttributes = [.foregroundColor : Colors.secondaryColor]
            navigationController.navigationBar.tintColor = Colors.secondaryColor
            navigationController.navigationBar.isTranslucent = false
        }
        
        //Load the JSON (Database) file from the server
        AF.request(Constants.serverDatabaseFile, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //Load all foods objects
                self.foods = json["foods"].arrayValue.map {
                    Food(json: $0)
                }
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foods.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell", for: indexPath) as! FoodCell
        cell.prepare(with: foods[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*Load the information view*/
        let informationsController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationsController") as! InformationsController
        informationsController.food = self.foods[indexPath.row]
        
        if let navigationController = self.navigationController {
            navigationController.pushViewController(informationsController, animated: true)
        }
    }
}
