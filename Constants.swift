//
//  Constants.swift
//  iOS Example
//
//  Created by Michaël Minelli on 25.10.17.
//  Copyright © 2017 Michaël Minelli. All rights reserved.
//

import Foundation

/// Global constants
public struct Constants {
    public static let serverURL = "https://minelli.me/hepia/pedagogy/ios_example/"
    
    public static let serverDatabaseFile = serverURL + "foods.json"
    public static let serverFoodPicturesDir = serverURL + "foods/"
}
